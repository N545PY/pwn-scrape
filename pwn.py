#!/usr/bin/env python3
import argparse
from time import sleep
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options

hide = Options()
hide.add_argument('--headless')
driver = webdriver.Firefox(options=hide)

def output(dict_in):
    for account, pwn_list in dict_in.items():
        if len(pwn_list) != 0:
            for pwn in pwn_list:
                print(Fore.GREEN + f"{account.rstrip()}:{pwn.rstrip()}")
        else:
            print(Fore.RED + f"{account} is not pwned")

# get account info via normal /account dir, may fail with the list option!

def get_page(account):
    driver.get(f"https://haveibeenpwned.com/account/{account}")
    results = driver.find_elements_by_class_name("pwnedCompanyTitle")
    pwn_list = []
    breach = {}
    for pwn in results:
        pwn = pwn.text
        if len(pwn) != 0:
            pwn_list.append(pwn)

    breach[account] = pwn_list
# lets try to use the search box to bypass cloudflare
def do_sneaky_get(account):
    driver.get("https://haveibeenpwned.com/")
    form = driver.find_element_by_id("Account")
    form.send_keys(account)
    form.send_keys(Keys.RETURN)
    sleep(5)
    results = driver.find_elements_by_class_name("pwnedCompanyTitle")
    pwn_list = []
    breach = {}
    for pwn in results:
        pwn = pwn.text
        if len(pwn) != 0:
            pwn_list.append(pwn)

    breach[account] = pwn_list
    return breach
def list_search(file_in, stealth=False, delay=20):
    with open(file_in, "r") as email_list:
        if stealth:
            for email in email_list:
                info = do_sneaky_get(email)
                output(info)
                sleep(delay)
        else:
            for email in email_list:
                info = get_page(email)
                output(info)
                sleep(delay)

def main():
   parser = argparse.ArgumentParser()
   #group = parser.add_mutually_exclusive_group()
   parser.add_argument("-e", "--email", help="Email to search haveibeenpwned")
   parser.add_argument("-l", "--list", help="file with emails to search haveibeenpwned, RATE LIMITED!")
   parser.add_argument("-p", "--proxy", help="HTTP/HTTPS Proxy to use")
   parser.add_argument("-v", "--version", help="Printse the version")
   parser.add_argument("-d", "--delay", default=20, type=int)
   parser.add_argument("-s", "--sneaky", action="store_true", default=False)
   args = parser.parse_args()
   if args.list is not None:
       list_search(args.list, args.sneaky, args.delay)
   else:
       if args.sneaky:
           info = do_sneaky_get(args.email)
           output(info)
       else:
           get_page(args.email)
   driver.quit()
   #do_sneaky_get("test@gmail.com")
main()
